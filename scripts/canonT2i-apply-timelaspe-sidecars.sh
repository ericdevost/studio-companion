#!/bin/bash
# This script will apply exposure compensation from the sidecars
# to rawtherapee pp3 files.

# Script has to be executed from the root of where the files, sidecars
# and processing profiles (pp3) are.

for sidecars in ./*.UFRAW; do
# for sidecars in ./IMG_0289.UFRAW; do
    echo $sidecars
    profiles=${sidecars/UFRAW/CR2.pp3}
    echo $profiles
    exposure=$(grep Exposure\> $sidecars | grep -Eo '[+-]?[0-9]+([.][0-9]+)?')
    echo $exposure
    sed -i "s/Compensation=0/Compensation=$exposure/" $profiles
done

# Process the files using sidecars and save them in folder
#mkdir rawtherapee-processed
#rawtherapee-cli -o rawtherapee-processed -Y -s -c ./
