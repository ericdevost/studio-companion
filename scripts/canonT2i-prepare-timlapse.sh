#!/bin/bash

# This script will download rename images from an sdcard
# and download them to a specified destination.

if [ $# -lt "1" ]; then
        echo "Usage: canonT2i-timelapse.sh <timelapse name>"
        exit 1;
fi
timelapsename=$1
destination='/mnt/qnap/photos/RawTimelapse'

# Checking if destination dir exists
if [ -d "$destination/$timelapsename" ] 
then
    echo "Exiting, destination directory exists."
    exit
else
    echo "Good to go, transefering images"
fi

# Without modification, this script will only run in
# very specific situations, as camera type,
# folder structure, your workflow and the naming scheme
# you want to use. Please modify it to suit your own needs.

# This script will MOVE images from the card, hence
# deleting them once they are copied to the destination

# This script has to be executed from the root of the DCIM/
# Directory on the card.

# This is specific to the Canon T2i camera
# The workflow has been inspired by:
# https://www.magiclantern.fm/forum/index.php?topic=5705.0

# Rename the files so it matches information in the sidecar
# This is specific to my camera, file naming is wrong.


find DCIM/ -depth -name '*.CR2' \
     -execdir bash -c 'mv -- "$1" "${1/_/I}"' bash {} \;
# Rename UFR to UFRAW to be able to process the images with UFRAW
find DCIM/ -name "*.UFR" -exec bash -c 'mv "$1" "${1%.UFR}".UFRAW' - '{}' \;
# Create destination directory
mkdir -p $destination/$timelapsename/ufrawconvert
# Move files and sidecars to the destination
find DCIM/ \( -iname '*.CR2' -o -iname '*.UFRAW' \) -type f -exec mv -nv -t $destination/$timelapsename -- {} +
#
