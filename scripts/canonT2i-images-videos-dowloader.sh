#!/bin/bash
####################################################
# ==================================================
# Images section
# ==================================================
####################################################

# Without modification, this script will only run in
# very specific situations, as camera type,
# folder structure, your workflow and the naming scheme
# you want to use. Please modify it to suit your own needs.

# This script will MOVE images from the card, hence
# deleting them once they are copied to the destination

# This script has to be executed from the root of the DCIM/
# Directory on the card.

# Get file number from filename, as it is not present in the image metadata
# This is specific to Canon 7D MII.

# Use Exiftool to change the image file names. 
exiftool '-filename<${CreateDate}-${filenumber}.%le' -d %y%m%d-%H%M -ext cr2 -r DCIM/
# Use Exiftool to change the system file modification date to match the image createdate:
exiftool '-filemodifydate<createdate' -ext cr2 -r DCIM/

# From this point forward, all Exiftool commands that write to the image file will
# require using the "-P" option at the beginning of the command, or else the system
# file modification date will change to the currect date and time.

# Use Exiftool to move the image files files to the appropriate album subfolders,
# creating new folders as needed: 
exiftool -P '-Directory<CreateDate' -d /mnt/photos/Raw/%y/%y%m \
         -ext cr2 -r DCIM/

######################################
# ====================================
# Video Section
# ====================================
######################################

# Use Exiftool to change the image file names:
exiftool '-filename<${CreateDate}-${filenumber}.%le' -d %y%m%d-%H%M -ext cr2 -r DCIM/
# Use Exiftool to change the system file modification date to match the image createdate:
exiftool '-filemodifydate<createdate' -ext MOV -r DCIM/

# From this point forward, all Exiftool commands that write to the image file will
# require using the "-P" option at the beginning of the command, or else the system
# file modification date will change to the currect date and time.

# Use Exiftool to move the image files files to the appropriate album subfolders,
# creating new folders as needed. The folder structure of the destination folder
# has been created to match my video editing workflow using the Power Sequencer
# addon of Blender.
# https://github.com/GDquest/Blender-power-sequencer
exiftool -P '-Directory<CreateDate' -d /mnt/videos/Raw/%y/%y%m%d/video \
         -ext MOV -r DCIM/
