# Taken from:
# https://medium.com/toutsbrasil/how-to-manage-your-dotfiles-with-git-f7aeed8adf8b

# First initial config
git init --bare $HOME/.dotfiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
dotfiles config --local status.showUntrackedFiles no
# add the alias to .bashrc (or .zshrc) so you can use it later
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
# Set the remote git origin to your git repo, then you
# can use for example
# -----------------------------------
# dotfiles status
# dotfiles add .vimrc
# dotfiles commit -m "Add vimrc"
# dotfiles add .bashrc
# dotfiles commit -m "Add bashrc"
# dotfiles push origin master

# For a new virgin system
# Clone your dotfiles repo
git clone --bare https://github.com/USERNAME/dotfiles.git $HOME/.dotfiles
# Define alias for current shell
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
# checkout the actual content from the git repository to your $HOME
dotfiles checkout
