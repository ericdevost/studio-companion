Studio Companion
================

This repository contains the configuration and
the scripts I used in my daily workflow when creating
audio, video or photography content.

Scripts
=======

photos-and-video-downloader.sh
------------------------------

Throughout the years, this has been by far the most useful
few lines of codes I've written. The script accomplish the
following tasks:

- Rename the image and videos files according to a specific scheme.
- Set the modification date to the image metadata to match image of video
  creation date.
- Transfer the files from my camera card to my local storage,
  creating directories as needed.

Studio configuration
====================

This is where I store my global studio configuration files. The
final objective is to have a full SaltStack recipe that will fully
configure my studio.

dotfiles.sh
-----------

Few lines of code to configure the tracking of my dotfiles under
git.
